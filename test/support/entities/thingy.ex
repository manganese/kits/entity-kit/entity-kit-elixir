defmodule Manganese.EntityKitTest.Support.Entities.Thingy do
  defstruct [
    :id,
    :widget_id,
    :color
  ]
end

defimpl Manganese.EntityKit.Protocols.ReloadableEntity, for: Manganese.EntityKitTest.Support.Entities.Thingy do
  def reload(entity, _repo), do: entity
end

defimpl Manganese.EntityKit.Protocols.SerializableEntity, for: Manganese.EntityKitTest.Support.Entities.Thingy do
  def serialize(entity) do
    %{
      "id" => entity.id,
      "widget_id" => entity.widget_id,
      "color" => "#" <> entity.color
    }
  end
end
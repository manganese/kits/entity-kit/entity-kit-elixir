defmodule Manganese.EntityKitTest.Support.Entities.Widget do
  defstruct [
    :id,
    :name,
    :description
  ]
end

defimpl Manganese.EntityKit.Protocols.ReloadableEntity, for: Manganese.EntityKitTest.Support.Entities.Widget do
  def reload(entity, _repo), do: entity
end

defimpl Manganese.EntityKit.Protocols.SerializableEntity, for: Manganese.EntityKitTest.Support.Entities.Widget do
  def serialize(entity) do
    %{
      "id" => entity.id,
      "name" => entity.name,
      "description" => entity.description
    }
  end
end
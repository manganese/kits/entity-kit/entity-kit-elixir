defmodule Manganese.EntityKitTest.Support.MockRepo do
  @moduledoc """

  """

  alias Manganese.EntityKit.Behaviors

  @behaviour Behaviors.Repo

  @impl true
  def reload_entity(entity, entity_type) do
    reloaded_entity =
      case entity_type do
        :accounts -> %{ entity | username: entity.username <> "-reloaded" }
        :assets ->   %{ entity | key: entity.key <> "-reloaded" }
      end

    { :ok, reloaded_entity }
  end

  @impl true
  def reload_entity!(entity, entity_type) do
    { :ok, reloaded_entity } = reload_entity entity_type, entity

    reloaded_entity
  end
end
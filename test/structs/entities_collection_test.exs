defmodule Manganese.EntityKitTest.Structs.EntitiesCollectionTest do
  use ExUnit.Case

  alias Manganese.EntityKitTest.Support

  alias Manganese.EntityKit.Protocols
  alias Manganese.EntityKit.Structs

  doctest Manganese.EntityKit.Structs.EntitiesCollection

  # Creation
  describe "`Manganese.EntityKit.Structs.EntitiesCollection.new/0`" do
    @tag entities_collection: true
    test "it returns an empty entities collection" do
      entities_collection = Structs.EntitiesCollection.new

      assert entities_collection == %Structs.EntitiesCollection{}
    end
  end

  # Deserialization
  describe "`Manganese.EntityKit.Structs.EntitiesCollection.from_map/2`" do
    @tag entities_collection: true
    test "it deserializes the entities collection from the given map" do
      serialized_account = %{
        "id" => 16,
        "username" => "sam",
        "created_at" => "2021-03-08T00:00:00.00Z"
      }
      deserializer = fn serialized_entity, _entity_type ->
        serialized_entity
        |> Map.new(fn { key, value } ->
          {
            String.to_atom(key),
            case key do
              "created_at" ->
                { :ok, created_at, _ } = DateTime.from_iso8601 value

                created_at
              _ -> value
            end
          }
        end)
      end
      deserialized_account = deserializer.(serialized_account, :accounts)

      map = %{
        "entities" => %{
          "accounts" => %{
            "16" => serialized_account
          }
        }
      }

      entities_collection = Structs.EntitiesCollection.from_map map, deserializer

      assert entities_collection == %Structs.EntitiesCollection{
        entities: %{
          accounts: %{
            16 => deserialized_account
          }
        }
      }
    end
  end

  # Getting
  describe "`Manganese.EntityKit.Structs.EntitiesCollection.get_entity/3`" do
    @tag entities_collection: true
    test "it returns the entity of the given type and ID from the entities collection when it is present" do
      account = %{
        id: 16,
        username: "sam"
      }
      entities_collection = %Structs.EntitiesCollection{
        entities: %{
          accounts: %{
            16 => account
          }
        }
      }

      assert Structs.EntitiesCollection.get_entity(entities_collection, :accounts, 16) == account
    end

    @tag entities_collection: true
    test "it returns `nil` when no entity of the given type and ID is present in the entities collection" do
      entities_collection = %Structs.EntitiesCollection{}

      assert Structs.EntitiesCollection.get_entity(entities_collection, :accounts, 16) == nil
    end
  end

  describe "`Manganese.EntityKit.Structs.EntitiesCollection.put_entity/3`" do
    @tag entities_collection: true
    test "it sets the entity of the given type in the entities collection" do
      account = %{
        id: 16,
        username: "sam"
      }
      entities_collection = %Structs.EntitiesCollection{}
      entities_collection = Structs.EntitiesCollection.put_entity entities_collection, :accounts, account

      assert entities_collection.entities[:accounts][16] == account
    end

    @tag entities_collection: true
    test "it overwrites the entity of the given type in the entities collection when it is already present" do
      account = %{
        id: 16,
        username: "sam"
      }
      entities_collection = %Structs.EntitiesCollection{
        entities: %{
          accounts: %{
            16 => account
          }
        }
      }

      updated_account = %{ account | username: "tom" }
      entities_collection = Structs.EntitiesCollection.put_entity entities_collection, :accounts, updated_account

      assert entities_collection.entities[:accounts][16] == updated_account
    end
  end

  describe "`Manganese.EntityKit.Structs.EntitiesCollection.put_entities/3`" do
    @tag entities_collection: true
    test "it sets the entities of the given type in the entities collection" do
      account1 = %{
        id: 16,
        username: "sam"
      }
      account2 = %{
        id: 17,
        username: "matt"
      }
      accounts = [ account1, account2 ]
      entities_collection = %Structs.EntitiesCollection{}
      entities_collection = Structs.EntitiesCollection.put_entities entities_collection, :accounts, accounts

      assert entities_collection.entities[:accounts][16] == account1
      assert entities_collection.entities[:accounts][17] == account2
    end
  end

  describe "`Manganese.EntityKit.Structs.EntitiesCollection.merge/2`" do
    @tag entities_collection: true
    test "it merges the entities collections" do
      account1 = %{
        id: 16,
        username: "sam"
      }
      account2 = %{
        id: 17,
        username: "matt"
      }
      asset1 = %{
        id: 32,
        key: "tree"
      }
      asset2 = %{
        id: 33,
        key: "rock"
      }
      entities_collection1 = %Structs.EntitiesCollection{
        entities: %{
          accounts: %{
            16 => account1
          },
          assets: %{
            33 => asset2
          }
        }
      }
      entities_collection2 = %Structs.EntitiesCollection{
        entities: %{
          accounts: %{
            17 => account2
          },
          assets: %{
            32 => asset1
          }
        }
      }
      entities_collection = Structs.EntitiesCollection.merge entities_collection1, entities_collection2

      assert entities_collection == %Structs.EntitiesCollection{
        entities: %{
          accounts: %{
            16 => account1,
            17 => account2
          },
          assets: %{
            32 => asset1,
            33 => asset2
          }
        }
      }
    end

    @tag entities_collection: true
    test "it overwrites entities with values from the second entities collection" do
      account1 = %{
        id: 16,
        username: "sam"
      }
      account2 = %{
        id: 16,
        username: "jackson"
      }
      entities_collection1 = %Structs.EntitiesCollection{
        entities: %{
          accounts: %{
            16 => account1
          }
        }
      }
      entities_collection2 = %Structs.EntitiesCollection{
        entities: %{
          accounts: %{
            16 => account2
          }
        }
      }
      entities_collection = Structs.EntitiesCollection.merge entities_collection1, entities_collection2

      assert entities_collection == %Structs.EntitiesCollection{
        entities: %{
          accounts: %{
            16 => account2
          }
        }
      }
    end
  end

  describe "`Manganese.EntityKit.Structs.EntitiesCollection.map/2`" do
    @tag entities_collection: true
    test "it maps each entity in the entities collection" do
      mapper = fn _entity_type, entity ->
        Map.new entity, fn { key, value } ->
          { Atom.to_string(key), value }
        end
      end
      entities_collection = %Structs.EntitiesCollection{
        entities: %{
          accounts: %{
            16 => %{
              id: 16,
              username: "sam"
            }
          },
          assets: %{
            33 => %{
              id: 33,
              key: "rock"
            }
          }
        }
      }
      |> Structs.EntitiesCollection.map(mapper)

      assert entities_collection == %Structs.EntitiesCollection{
        entities: %{
          accounts: %{
            16 => %{
              "id" => 16,
              "username" => "sam"
            }
          },
          assets: %{
            33 => %{
              "id" => 33,
              "key" => "rock"
            }
          }
        }
      }
    end
  end

  describe "`Manganese.EntityKit.Protocols.FilterableCollection.filter/2, for: Manganese.EntityKit.Structs.EntitiesCollection`" do
    @tag entities_collection: true, filterable: true
    test "it filters the entities in the entities collection" do
      entities_collection =
        %Structs.EntitiesCollection{
          entities: %{
            accounts: %{
              16 => %{
                id: 16,
                username: "sam"
              },
              117 => %{
                id: 117,
                username: "mark"
              }
            },
            assets: %{
              33 => %{
                id: 33,
                key: "rock"
              },
              129 => %{
                id: 129,
                key: "lantern"
              }
            },
            worlds: %{
              210 => %{
                id: 210,
                key: "jupiter"
              }
            }
          }
        }
        |> Protocols.FilterableCollection.filter(fn _entity_type, id ->
          id < 100
        end)

      assert entities_collection == %Structs.EntitiesCollection{
        entities: %{
          accounts: %{
            16 => %{
              id: 16,
              username: "sam"
            }
          },
          assets: %{
            33 => %{
              id: 33,
              key: "rock"
            }
          }
        }
      }
    end
  end

  describe "`Manganese.EntityKit.Protocols.ReloadableCollection.reload/2, for: Manganese.EntityKit.Structs.EntitiesCollection`" do
    @tag entities_collection: true, reloadable: true
    test "it reloads each entity in the entities collection" do
      { :ok, entities_collection } =
        %Structs.EntitiesCollection{
          entities: %{
            accounts: %{
              16 => %{
                id: 16,
                username: "sam"
              }
            },
            assets: %{
              33 => %{
                id: 33,
                key: "rock"
              }
            }
          }
        }
        |> Protocols.ReloadableCollection.reload(Support.MockRepo)

      assert entities_collection == %Structs.EntitiesCollection{
        entities: %{
          accounts: %{
            16 => %{
              id: 16,
              username: "sam-reloaded"
            }
          },
          assets: %{
            33 => %{
              id: 33,
              key: "rock-reloaded"
            }
          }
        }
      }
    end
  end

  describe "`Manganese.EntityKit.Protocols.SerializableCollection.serialize/2, for: Manganese.EntityKit.Structs.EntitiesCollection`" do
    @tag entities_collection: true, serializable: true
    test "it serializes each entity in the entities collection" do
      entities_collection = %Structs.EntitiesCollection{
        entities: %{
          accounts: %{
            16 => %{
              id: 16,
              username: "sam"
            }
          },
          assets: %{
            33 => %{
              id: 33,
              key: "rock"
            }
          }
        }
      }
      serializer = fn entity, _entity_type ->
        Map.new entity, fn { key, value } ->
          { Atom.to_string(key), value }
        end
      end

      map =
        Protocols.SerializableCollection.serialize entities_collection, serializer

      assert map == %{
        "entities" => %{
          "accounts" => %{
            16 => %{
              "id" => 16,
              "username" => "sam"
            }
          },
          "assets" => %{
            33 => %{
              "id" => 33,
              "key" => "rock"
            }
          }
        }
      }
    end
  end
end

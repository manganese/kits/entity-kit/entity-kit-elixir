defmodule Manganese.EntityKitTest.Structs.EntitiesUpdateTest do
  use ExUnit.Case

  alias Manganese.EntityKitTest.Support

  alias Manganese.EntityKit.Protocols
  alias Manganese.EntityKit.Structs

  doctest Manganese.EntityKit.Structs.EntitiesUpdate

  # Creation
  describe "`Manganese.EntityKit.Structs.EntitiesUpdate.new/0`" do
    @tag entities_update: true
    test "it returns an empty entities update" do
      entities_update = Structs.EntitiesUpdate.new

      assert entities_update == %Structs.EntitiesUpdate{}
    end
  end

  # Deserialization
  describe "`Manganese.EntityKit.Structs.EntitiesUpdate.from_map/2`" do
    @tag entities_collection: true
    test "it deserializes the entities collection from the given map" do
      serialized_account = %{
        "id" => 16,
        "username" => "sam",
        "created_at" => "2021-03-08T00:00:00.00Z"
      }
      deserializer = fn serialized_entity, _entity_type ->
        serialized_entity
        |> Map.new(fn { key, value } ->
          {
            String.to_atom(key),
            case key do
              "created_at" ->
                { :ok, created_at, _ } = DateTime.from_iso8601 value

                created_at
              _ -> value
            end
          }
        end)
      end
      deserialized_account = deserializer.(serialized_account, :accounts)

      ids = [
        17,
        18
      ]

      map = %{
        "updated_entities" => %{
          "entities" => %{
            "accounts" => %{
              "16" => serialized_account
            }
          }
        },
        "removed_entities" => %{
          "entity_ids" => %{
            "accounts" => ids
          }
        }
      }

      entities_collection = Structs.EntitiesUpdate.from_map map, deserializer

      assert entities_collection == %Structs.EntitiesUpdate{
        updated_entities: %Structs.EntitiesCollection{
          entities: %{
            accounts: %{
              16 => deserialized_account
            }
          }
        },
        removed_entities: %Structs.EntityReferencesCollection{
          entity_ids: %{
            accounts: ids
          }
        }
      }
    end
  end

  describe "`Manganese.EntityKit.Structs.EntitiesUpdate.put_entity/3`" do
    @tag entities_update: true
    test "it sets the entity of the given type in the entities update's updated entities" do
      account = %{
        id: 16,
        username: "sam"
      }
      entities_update =
        %Structs.EntitiesUpdate{}
        |> Structs.EntitiesUpdate.put_entity(:accounts, account)

      assert entities_update.updated_entities == (
        Structs.EntitiesCollection.new
        |> Structs.EntitiesCollection.put_entity(:accounts, account)
      )
    end
  end

  describe "`Manganese.EntityKit.Structs.EntitiesUpdate.put_entities/3`" do
    @tag entities_update: true
    test "it sets the entities of the given type in the entities update's updated entities" do
      account1 = %{
        id: 16,
        username: "sam"
      }
      account2 = %{
        id: 17,
        username: "matt"
      }
      accounts = [ account1, account2 ]
      entities_update =
        %Structs.EntitiesUpdate{}
        |> Structs.EntitiesUpdate.put_entities(:accounts, accounts)

      assert entities_update.updated_entities == (
        Structs.EntitiesCollection.new
        |> Structs.EntitiesCollection.put_entities(:accounts, accounts)
      )
    end
  end

  describe "`Manganese.EntityKit.Structs.EntitiesUpdate.put_entities_collection/2`" do
    @tag entities_update: true
    test "it merges the given entities collection with the entities update's updated entities" do
      account1 = %{
        id: 16,
        username: "sam"
      }
      account2 = %{
        id: 17,
        username: "james"
      }

      entities_collection1 =
        Structs.EntitiesCollection.new
        |> Structs.EntitiesCollection.put_entity(:accounts, account1)

      entities_collection2 =
        Structs.EntitiesCollection.new
        |> Structs.EntitiesCollection.put_entity(:accounts, account2)

      entities_update = %Structs.EntitiesUpdate{
        updated_entities: entities_collection1
      }
      |> Structs.EntitiesUpdate.put_entities_collection(entities_collection2)

      assert entities_update.updated_entities == Structs.EntitiesCollection.merge entities_collection1, entities_collection2
    end
  end

  describe "`Manganese.EntityKit.Structs.EntitiesUpdate.put_removed_entity_id/3`" do
    @tag entities_update: true
    test "it adds the entity ID of the given type to the entities update's removed entities" do
      account_id = 16
      entities_update =
        %Structs.EntitiesUpdate{}
        |> Structs.EntitiesUpdate.put_removed_entity_id(:accounts, account_id)

      assert entities_update.removed_entities == (
        Structs.EntityReferencesCollection.new
        |> Structs.EntityReferencesCollection.put_entity_id(:accounts, account_id)
      )
    end
  end

  describe "`Manganese.EntityKit.Structs.EntitiesUpdate.put_removed_entity_ids/3`" do
    @tag entities_update: true
    test "it adds the entity IDs of the given type to the entities update's removed entities" do
      account_ids = [ 16, 17 ]
      entities_update =
        %Structs.EntitiesUpdate{}
        |> Structs.EntitiesUpdate.put_removed_entity_ids(:accounts, account_ids)

      assert entities_update.removed_entities == (
        Structs.EntityReferencesCollection.new
        |> Structs.EntityReferencesCollection.put_entity_ids(:accounts, account_ids)
      )
    end
  end

  describe "`Manganese.EntityKit.Structs.EntitiesUpdate.put_removed_entity/3`" do
    @tag entities_update: true
    test "it adds the ID of the entity of the given type to the entities update's removed entities" do
      account = %{
        id: 16,
        username: "sam"
      }
      entities_update =
        %Structs.EntitiesUpdate{}
        |> Structs.EntitiesUpdate.put_removed_entity(:accounts, account)

      assert entities_update.removed_entities == (
        Structs.EntityReferencesCollection.new
        |> Structs.EntityReferencesCollection.put_entity_id(:accounts, account.id)
      )
    end
  end

  describe "`Manganese.EntityKit.Structs.EntitiesUpdate.put_removed_entities/3`" do
    @tag entities_update: true
    test "it adds the IDs of the entities of the given type to the entities update's removed entities" do
      account1 = %{
        id: 16,
        username: "sam"
      }
      account2 = %{
        id: 17,
        username: "matt"
      }
      accounts = [ account1, account2 ]
      entities_update =
        %Structs.EntitiesUpdate{}
        |> Structs.EntitiesUpdate.put_removed_entities(:accounts, accounts)

      assert entities_update.removed_entities == (
        Structs.EntityReferencesCollection.new
        |> Structs.EntityReferencesCollection.put_entity_ids(
          :accounts,
          accounts
          |> Enum.map(fn entity -> entity.id end)
        )
      )
    end
  end

  describe "`Manganese.EntityKit.Structs.EntitiesUpdate.put_removed_entity_references_collection/3`" do
    @tag entities_update: true
    test "it merges the given entity references collection with the entities update's removed entities" do
      entity_references_collection1 =
        Structs.EntityReferencesCollection.new
        |> Structs.EntityReferencesCollection.put_entity_ids(:accounts, [ 16, 17 ])

      entity_references_collection2 =
        Structs.EntityReferencesCollection.new
        |> Structs.EntityReferencesCollection.put_entity_ids(:accounts, [ 18, 19 ])

      entities_update =
        %Structs.EntitiesUpdate{
          removed_entities: entity_references_collection1
        }
        |> Structs.EntitiesUpdate.put_removed_entity_references_collection(entity_references_collection2)

      assert entities_update.removed_entities == (
        Structs.EntityReferencesCollection.merge(
          entity_references_collection1,
          entity_references_collection2
        )
      )
    end
  end

  describe "`Manganese.EntityKit.Structs.EntitiesUpdate.merge/2`" do
    @tag entities_update: true
    test "it merges the entities updates" do
      account1 = %{
        id: 16,
        username: "sam"
      }
      account2 = %{
        id: 17,
        username: "matt"
      }
      asset1 = %{
        id: 32,
        key: "tree"
      }
      asset2 = %{
        id: 33,
        key: "rock"
      }
      entities_update1 = %Structs.EntitiesUpdate{
        updated_entities:
          Structs.EntitiesCollection.new
          |> Structs.EntitiesCollection.put_entity(:accounts, account1)
          |> Structs.EntitiesCollection.put_entity(:assets, asset2),
        removed_entities:
          Structs.EntityReferencesCollection.new
          |> Structs.EntityReferencesCollection.put_entity_id(:assets, 34)
      }
      entities_update2 = %Structs.EntitiesUpdate{
        updated_entities:
          Structs.EntitiesCollection.new
          |> Structs.EntitiesCollection.put_entity(:accounts, account2)
          |> Structs.EntitiesCollection.put_entity(:assets, asset1),
        removed_entities:
          Structs.EntityReferencesCollection.new
          |> Structs.EntityReferencesCollection.put_entity_id(:assets, 35)
          |> Structs.EntityReferencesCollection.put_entity_id(:worlds, 84)
      }

      entities_update = Structs.EntitiesUpdate.merge entities_update1, entities_update2

      assert entities_update == %Structs.EntitiesUpdate{
        updated_entities:
          Structs.EntitiesCollection.new
          |> Structs.EntitiesCollection.put_entities(:accounts, [ account1, account2 ])
          |> Structs.EntitiesCollection.put_entities(:assets, [ asset2, asset1 ]),
        removed_entities:
          Structs.EntityReferencesCollection.new
          |> Structs.EntityReferencesCollection.put_entity_ids(:assets, [ 34, 35 ])
          |> Structs.EntityReferencesCollection.put_entity_id(:worlds, 84)
      }
    end
  end

  describe "`Manganese.EntityKit.Protocols.FilterableCollection.filter/2, for: Manganese.EntityKit.Structs.EntitiesUpdate`" do
    @tag filterable: true, entities_update: true
    @tag entities_update: true
    test "it filters each entity in the entities update" do
      account1 = %{
        id: 16,
        username: "sam"
      }
      account2 = %{
        id: 116,
        username: "calvin"
      }
      entities_collection =
        Structs.EntitiesCollection.new
        |> Structs.EntitiesCollection.put_entities(:accounts, [ account1, account2 ])
      entity_references_collection =
        Structs.EntityReferencesCollection.new
        |> Structs.EntityReferencesCollection.put_entity_ids(:assets, [ 32, 132, 133 ])

      filter = fn _entity_type, id ->
        id < 100
      end

      entities_update = %Structs.EntitiesUpdate{
        updated_entities: entities_collection,
        removed_entities: entity_references_collection
      }
      |> Protocols.FilterableCollection.filter(filter)

      assert entities_update == %Structs.EntitiesUpdate{
        updated_entities:
          entities_collection
          |> Protocols.FilterableCollection.filter(filter),
        removed_entities:
          entity_references_collection
          |> Protocols.FilterableCollection.filter(filter)
      }
    end
  end

  describe "`Manganese.EntityKit.Protocols.SerializableCollection.serialize/2, for: Manganese.EntityKit.Structs.EntitiesUpdate`" do
    @tag serializable: true, entities_update: true
    test "it serializes each entity in the entities update" do
      account1 = %{
        id: 16,
        username: "sam"
      }
      account2 = %{
        id: 17,
        username: "tom"
      }
      entities_collection =
        Structs.EntitiesCollection.new
        |> Structs.EntitiesCollection.put_entities(:accounts, [ account1, account2 ])
      entity_references_collection =
        Structs.EntityReferencesCollection.new
        |> Structs.EntityReferencesCollection.put_entity_ids(:assets, [ 32, 33 ])

      serializer = fn entity, _entity_type ->
        Map.new entity, fn { key, value } ->
          { Atom.to_string(key), value }
        end
      end

      entities_update = %Structs.EntitiesUpdate{
        updated_entities: entities_collection,
        removed_entities: entity_references_collection
      }
      |> Protocols.SerializableCollection.serialize(serializer)

      assert entities_update == %{
        "updated_entities" =>
          entities_collection
          |> Protocols.SerializableCollection.serialize(serializer),
        "removed_entities" =>
          entity_references_collection
          |> Structs.EntityReferencesCollection.to_map
      }
    end
  end

  describe "`Manganese.EntityKit.Protocols.ReloadableCollection.reload/2, for: Manganese.EntityKit.Structs.EntitiesUpdate`" do
    @tag reloadable: true, entities_update: true
    test "it reloads each entity in the entities update" do
      account1 = %{
        id: 16,
        username: "sam"
      }
      asset1 = %{
        id: 65,
        key: "sword"
      }
      entities_collection =
        Structs.EntitiesCollection.new
        |> Structs.EntitiesCollection.put_entity(:accounts, account1)
        |> Structs.EntitiesCollection.put_entity(:assets, asset1)
      entity_references_collection =
        Structs.EntityReferencesCollection.new
        |> Structs.EntityReferencesCollection.put_entity_ids(:worlds, [ 88, 89 ])

      entities_update = %Structs.EntitiesUpdate{
        updated_entities: entities_collection,
        removed_entities: entity_references_collection
      }
      |> Protocols.ReloadableCollection.reload(Support.MockRepo)

      assert entities_update == %Structs.EntitiesUpdate{
        updated_entities:
          entities_collection
          |> Protocols.ReloadableCollection.reload(Support.MockRepo),
        removed_entities: entity_references_collection
      }
    end
  end
end

defmodule Manganese.EntityKitTest.Structs.EntityReferencesCollectionTest do
  use ExUnit.Case

  alias Manganese.EntityKit.Protocols
  alias Manganese.EntityKit.Structs

  doctest Manganese.EntityKit.Structs.EntityReferencesCollection

  # Creation
  describe "`Manganese.EntityKit.Structs.EntityReferencesCollection.new/0`" do
    @tag entity_references_collection: true
    test "it returns an empty entity references collection" do
      entity_references_collection = Structs.EntityReferencesCollection.new

      assert entity_references_collection == %Structs.EntityReferencesCollection{}
    end
  end

  # Deserialization
  describe "`Manganese.EntityKit.Structs.EntityReferencesCollection.from_map/1`" do
    @tag entity_references_collection: true
    test "it deserializes the entity references collection from the given map" do
      ids = [
        16,
        17,
        18
      ]
      map = %{
        "entity_ids" => %{
          "accounts" => ids
        }
      }

      entity_references_collection = Structs.EntityReferencesCollection.from_map map

      assert entity_references_collection == %Structs.EntityReferencesCollection{
        entity_ids: %{
          accounts: ids
        }
      }
    end
  end

  describe "`Manganese.EntityKit.Structs.EntityReferencesCollection.has_entity?/3`" do
    @tag entity_references_collection: true
    test "it returns true when the entity of the given type and ID is present" do
      entity_references_collection = %Structs.EntityReferencesCollection{
        entity_ids: %{
          accounts: [ 16 ]
        }
      }

      assert Structs.EntityReferencesCollection.has_entity?(entity_references_collection, :accounts, 16) == true
    end

    @tag entity_references_collection: true
    test "it returns false when the entity of the given type and ID is not present" do
      entity_references_collection = %Structs.EntityReferencesCollection{}

      assert Structs.EntityReferencesCollection.has_entity?(entity_references_collection, :accounts, 16) == false
    end
  end

  describe "`Manganese.EntityKit.Structs.EntityReferencesCollection.put_entity_id/3`" do
    @tag entity_references_collection: true
    test "it adds the entity ID of the given type to the entity references collection" do
      entity_references_collection = %Structs.EntityReferencesCollection{}
      entity_references_collection =
        entity_references_collection
        |> Structs.EntityReferencesCollection.put_entity_id(:accounts, 17)

      assert entity_references_collection.entity_ids[:accounts] == [ 17 ]
    end

    @tag entity_references_collection: true
    test "it does not add the entity ID of the given type to the entity references collection when it is already present" do
      entity_references_collection =
        Enum.reduce(
          1 .. 100,
          %Structs.EntityReferencesCollection{},
          fn _, entity_references_collection ->
            entity_references_collection
            |> Structs.EntityReferencesCollection.put_entity_id(:accounts, 17)
          end
        )

      assert entity_references_collection.entity_ids[:accounts] == [ 17 ]
    end
  end

  describe "`Manganese.EntityKit.Structs.EntityReferencesCollection.put_entity_ids/3`" do
    @tag entity_references_collection: true
    test "it adds the entity IDs of the given type to the entity references collection" do
      entity_references_collection = %Structs.EntityReferencesCollection{}
      entity_references_collection =
        entity_references_collection
        |> Structs.EntityReferencesCollection.put_entity_ids(:accounts, [ 16, 17, 18 ])

      assert entity_references_collection.entity_ids[:accounts] == [ 16, 17, 18 ]
    end
  end

  describe "`Manganese.EntityKit.Structs.EntityReferencesCollection.merge/2`" do
    @tag entity_references_collection: true
    test "it merges the entity references collections" do
      entity_references_collection1 = %Structs.EntityReferencesCollection{
        entity_ids: %{
          accounts: [ 16 ],
          assets: [ 33 ]
        }
      }
      entity_references_collection2 = %Structs.EntityReferencesCollection{
        entity_ids: %{
          accounts: [ 17 ],
          assets: [ 32 ]
        }
      }
      entity_references_collection = Structs.EntityReferencesCollection.merge entity_references_collection1, entity_references_collection2

      assert entity_references_collection == %Structs.EntityReferencesCollection{
        entity_ids: %{
          accounts: [ 16, 17 ],
          assets: [ 33, 32 ]
        }
      }
    end
  end

  describe "`Manganese.EntityKit.Structs.EntityReferencesCollection.to_map/1`" do
    @tag entity_references_collection: true
    test "it converts the entity references collection to a map" do
      entity_references_collection = %Structs.EntityReferencesCollection{
        entity_ids: %{
          accounts: [ 16, 17 ],
          assets: [ 32, 33, 34 ]
        }
      }
      map =
        entity_references_collection
        |> Structs.EntityReferencesCollection.to_map

      assert map == %{
        "entity_ids" => %{
          "accounts" => [ 16, 17 ],
          "assets" => [ 32, 33, 34 ]
        }
      }
    end
  end

  describe "`Manganese.EntityKit.Protocols.FilterableCollection.filter/2, for: Manganese.EntityKit.Structs.EntityReferencesCollection`" do
    @tag entity_references_collection: true, filterable: true
    test "it filters the entity IDs" do
      filter = fn _entity_type, entity_id ->
        entity_id < 100
      end
      entity_references_collection =
        %Structs.EntityReferencesCollection{
          entity_ids: %{
            accounts: [ 16, 116 ],
            assets: [ 32, 132 ],
            worlds: [ 176 ]
          }
        }
        |> Protocols.FilterableCollection.filter(filter)

      assert entity_references_collection == %Structs.EntityReferencesCollection{
        entity_ids: %{
          accounts: [ 16 ],
          assets: [ 32 ]
        }
      }
    end
  end
end

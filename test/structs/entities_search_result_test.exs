defmodule Manganese.EntityKitTest.Structs.EntitiesSearchResultTest do
  use ExUnit.Case

  alias Manganese.EntityKitTest.Support

  alias Manganese.EntityKit.Protocols
  alias Manganese.EntityKit.Structs

  doctest Manganese.EntityKit.Structs.EntitiesSearchResult

  # Creation
  describe "`Manganese.EntityKit.Structs.EntitiesSearchResult.new/0`" do
    @tag entities_search_result: true
    test "it returns an empty entities collection" do
      entities_search_result = Structs.EntitiesSearchResult.new

      assert entities_search_result == %Structs.EntitiesSearchResult{}
    end
  end

  describe "`Manganese.EntityKit.Structs.EntitiesSearchResult.put_entity/3`" do
    @tag entities_search_result: true
    test "it sets the entity of the given type in the entities search result's entities collection and adds it to the list of entity IDs" do
      account = %{
        id: 16,
        username: "sam"
      }
      entities_search_result =
        %Structs.EntitiesSearchResult{}
        |> Structs.EntitiesSearchResult.put_entity(:accounts, account)

      assert entities_search_result == %Structs.EntitiesSearchResult{
        entities_collection:
          Structs.EntitiesCollection.new
          |> Structs.EntitiesCollection.put_entity(:accounts, account),
        entity_ids: [ account.id ],
        count: 1
      }
    end

    @tag entities_search_result: true
    test "it increments the count of the entities when the entities search result's list of entity IDs is greater than the original count" do
      account1 = %{
        id: 16,
        username: "sam"
      }
      account2 = %{
        id: 17,
        username: "joseph"
      }
      entities_search_result =
        %Structs.EntitiesSearchResult{}
        |> Structs.EntitiesSearchResult.put_entity(:accounts, account1)
        |> Structs.EntitiesSearchResult.put_entity(:accounts, account2)

      assert entities_search_result.count == 2
    end

    @tag entities_search_result: true
    test "it does not increment the count of the entities when the entities search result's list of entity IDs is less than or equal to the original count" do
      account1 = %{
        id: 16,
        username: "sam"
      }
      account2 = %{
        id: 17,
        username: "joseph"
      }
      entities_search_result =
        %Structs.EntitiesSearchResult{
          count: 37
        }
        |> Structs.EntitiesSearchResult.put_entity(:accounts, account1)
        |> Structs.EntitiesSearchResult.put_entity(:accounts, account2)

      assert entities_search_result.count == 37
    end
  end

  describe "`Manganese.EntityKit.Structs.EntitiesSearchResult.put_entities/3`" do
    @tag entities_search_result: true
    test "it sets the entities of the given type in the entities search result's entities collection and adds them to the list of entity IDs" do
      account1 = %{
        id: 16,
        username: "sam"
      }
      account2 = %{
        id: 17,
        username: "tom"
      }
      accounts = [ account1, account2 ]
      entities_search_result =
        %Structs.EntitiesSearchResult{}
        |> Structs.EntitiesSearchResult.put_entities(:accounts, accounts)

      assert entities_search_result == %Structs.EntitiesSearchResult{
        entities_collection:
          Structs.EntitiesCollection.new
          |> Structs.EntitiesCollection.put_entities(:accounts, accounts),
        entity_ids:
          accounts
          |> Enum.map(fn entity -> entity.id end),
        count: 2
      }
    end

    @tag entities_search_result: true
    test "it increments the count of the entities when the entities search result's list of entity IDs is greater than the original count" do
      account1 = %{
        id: 16,
        username: "sam"
      }
      account2 = %{
        id: 17,
        username: "joseph"
      }
      accounts = [ account1, account2 ]
      entities_search_result =
        %Structs.EntitiesSearchResult{}
        |> Structs.EntitiesSearchResult.put_entities(:accounts, accounts)

      assert entities_search_result.count == 2
    end

    @tag entities_search_result: true
    test "it does not increment the count of the entities when the entities search result's list of entity IDs is less than or equal to the original count" do
      account1 = %{
        id: 16,
        username: "sam"
      }
      account2 = %{
        id: 17,
        username: "joseph"
      }
      accounts = [ account1, account2 ]
      entities_search_result =
        %Structs.EntitiesSearchResult{
          count: 38
        }
        |> Structs.EntitiesSearchResult.put_entities(:accounts, accounts)

      assert entities_search_result.count == 38
    end
  end

  describe "`Manganese.EntityKit.Structs.EntitiesSearchResult.put_parameters/2`" do
    @tag entities_search_result: true
    test "it overwrites the parameters in the entities search result" do
      parameters = %{
        query: "ryan"
      }
      entities_search_result =
        %Structs.EntitiesSearchResult{}
        |> Structs.EntitiesSearchResult.put_parameters(parameters)

      assert entities_search_result.parameters == parameters
    end
  end

  describe "`Manganese.EntityKit.Structs.EntitiesSearchResult.put_count/2`" do
    @tag entities_search_result: true
    test "it overwrites the count in the entities search result" do
      count = 71
      entities_search_result =
        %Structs.EntitiesSearchResult{}
        |> Structs.EntitiesSearchResult.put_count(count)

      assert entities_search_result.count == count
    end
  end

  describe "`Manganese.EntityKit.Protocols.ReloadableCollection.reload/2, for: Manganese.EntityKit.Structs.EntitiesSearchResult`" do
    @tag entities_search_result: true, reloadable: true
    test "it reloads each entity in the entities search result" do
      asset = %{
        id: 96,
        key: "lilac-flowers"
      }
      entities_collection =
        Structs.EntitiesCollection.new
        |> Structs.EntitiesCollection.put_entity(:assets, asset)
      count = 71
      parameters = %{
        query: "lilac"
      }
      entities_search_result =
        %Structs.EntitiesSearchResult{
          entities_collection: entities_collection,
          entity_ids: [ asset.id ],
          count: count,
          parameters: parameters
        }
        |> Protocols.ReloadableCollection.reload(Support.MockRepo)

      assert entities_search_result == %Structs.EntitiesSearchResult{
        entities_collection:
          entities_collection
          |> Protocols.ReloadableCollection.reload(Support.MockRepo),
        entity_ids: [ asset.id ],
        count: count,
        parameters: parameters
      }
    end
  end

  describe "`Manganese.EntityKit.Protocols.SerializableCollection.serialize/2, for: Manganese.EntityKit.Structs.EntitiesSearchResult`" do
    @tag entities_search_result: true, serializable: true
    test "it serializes each entity in the entities search result" do
      asset = %{
        id: 96,
        key: "lilac-flowers"
      }
      entities_collection =
        Structs.EntitiesCollection.new
        |> Structs.EntitiesCollection.put_entity(:assets, asset)
      count = 71
      parameters = %{
        query: "lilac"
      }
      entities_search_result =
        %Structs.EntitiesSearchResult{
          entities_collection: entities_collection,
          entity_ids: [ asset.id ],
          count: count,
          parameters: parameters
        }
      serializer = fn entity, _entity_type ->
        Map.new entity, fn { key, value } ->
          { Atom.to_string(key), value }
        end
      end

      map =
        entities_search_result
        |> Protocols.SerializableCollection.serialize(serializer)

      assert map == %{
        "entities_collection" =>
          entities_collection
          |> Protocols.SerializableCollection.serialize(serializer),
        "entity_ids" => [ asset.id ],
        "count" => count,
        "parameters" => parameters
      }
    end
  end
end

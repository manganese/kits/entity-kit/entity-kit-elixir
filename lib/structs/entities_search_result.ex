defmodule Manganese.EntityKit.Structs.EntitiesSearchResult do
  import Ecto.Query

  alias Manganese.CoreKit
  alias Manganese.EntityKit.Structs

  @typedoc """

  """
  @type t :: %Structs.EntitiesSearchResult{
    entities_collection: Structs.EntitiesCollection.t,
    entity_ids: [ integer ],
    parameters: term,
    count: non_neg_integer
  }
  defstruct [
    entities_collection: Structs.EntitiesCollection.new,
    entity_ids: [],
    parameters: nil,
    count: 0
  ]

  # Utilities

  @doc """

  """
  @spec new :: t
  def new, do: %Structs.EntitiesSearchResult{}

  @doc """

  """
  @spec put_entity(t, atom, term) :: t
  def put_entity(search_result, entity_type, entity) do
    entity_ids =
      (search_result.entity_ids ++ [ entity.id ])
      |> Enum.uniq

    %{
      search_result |
      entities_collection:
        search_result.entities_collection
        |> Structs.EntitiesCollection.put_entity(entity_type, entity),
      entity_ids: entity_ids,
      count: max(search_result.count, length(entity_ids))
    }
  end

  @doc """

  """
  @spec put_entities(t, atom, [ term ]) :: t
  def put_entities(search_result, entity_type, entities) do
    entity_ids =
      (
        search_result.entity_ids
        ++
        entities
        |> Enum.map(fn entity -> entity.id end)
      )
      |> Enum.uniq

    %{
      search_result |
      entities_collection:
        search_result.entities_collection
        |> Structs.EntitiesCollection.put_entities(entity_type, entities),
      entity_ids: entity_ids,
      count: max(search_result.count, length(entity_ids))
    }
  end

  @doc """

  """
  def put_parameters(search_result, parameters) do
    %{ search_result | parameters: parameters }
  end

  @doc """

  """
  def put_count(search_result, count) do
    %{ search_result | count: count }
  end

  @doc """

  """
  @spec from_query(Ecto.Query.t, atom, term) :: { :ok, t } | { :error, CoreKit.Structs.APIError.t }
  def from_query(ecto_query, entity_type, parameters \\ %{}) do
    case {
      Repo.all(ecto_query),
      Repo.one(
        ecto_query
        |> exclude(:limit)
        |> exclude(:offset)
        |> select(fragment "count(*)")
      )
    } do
      { entities, count } when is_list(entities) ->
        search_result =
          EntityKit.Structs.EntitiesSearchResult.new
          |> EntityKit.Structs.EntitiesSearchResult.put_parameters(parameters)
          |> EntityKit.Structs.EntitiesSearchResult.put_count(count)
          |> EntityKit.Structs.EntitiesSearchResult.put_entities(entity_type, entities)

        { :ok, search_result }
      {{ :error, _ }, _ } ->
        { :error, %CoreKit.Structs.APIError{
          type: :database_error,
          status: :service_unavailable
        }}
    end
  end
end

defimpl Manganese.EntityKit.Protocols.ReloadableCollection, for: Manganese.EntityKit.Structs.EntitiesSearchResult do
  alias Manganese.EntityKit.Protocols
  alias Manganese.EntityKit.Structs

  def reload(search_result, repo) do
    %Structs.EntitiesSearchResult{
      search_result |
      entities_collection:
        search_result.entities_collection
        |> Protocols.ReloadableCollection.reload(repo)
    }
  end
end

defimpl Manganese.EntityKit.Protocols.SerializableCollection, for: Manganese.EntityKit.Structs.EntitiesSearchResult do
  alias Manganese.EntityKit.Protocols

  def serialize(search_result, serializer) do
    %{
      "entities_collection" =>
        search_result.entities_collection
        |> Protocols.SerializableCollection.serialize(serializer),
      "entity_ids" => search_result.entity_ids,
      "parameters" => search_result.parameters,
      "count" => search_result.count
    }
  end
end

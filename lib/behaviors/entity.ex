defmodule Manganese.EntityKit.Behaviors.Entity do
  @moduledoc """


  """

  alias Manganese.CoreKit

  alias Manganese.EntityKit.Protocols

  @typedoc """

  """
  @type id :: pos_integer

  @typedoc """

  """
  @type t :: term


  @doc """

  """
  @spec serialize(t, atom, (t, atom -> map)) :: { :ok, t } | { :error, CoreKit.Structs.APIError.t }
  def serialize(entity, entity_type, serializer) do
    try do
      entity
      |> Protocols.SerializableEntity.serialize(entity_type)
    rescue
      Protocol.UndefinedError ->
        serializer.(entity, entity_type)
    end
  end

  @doc """

  """
  @spec reload(t, atom, module) :: { :ok, t } | { :error, CoreKit.Structs.APIError.t }
  def reload(entity, entity_type, repo) do
    try do
      entity
      |> Protocols.ReloadableEntity.reload(entity_type, repo)
    rescue
      Protocol.UndefinedError ->
        repo.reload_entity entity, entity_type
    end
  end
end
defmodule Manganese.EntityKit.Behaviors.Repo do
  alias Manganese.CoreKit

  @callback reload_entity(term, atom) :: { :ok, term } | { :error, CoreKit.Structs.APIError.t }
  @callback reload_entity!(term, atom) :: term
end
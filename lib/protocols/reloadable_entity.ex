defprotocol Manganese.EntityKit.Protocols.ReloadableEntity do
  alias Manganese.CoreKit

  @doc """

  """
  @spec reload(t, atom, module) :: { :ok, t } | { :error, CoreKit.Structs.APIError.t }
  def reload(entity, entity_type, repo)
end
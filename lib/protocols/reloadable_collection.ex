defprotocol Manganese.EntityKit.Protocols.ReloadableCollection do
  alias Manganese.CoreKit

  @doc """

  """
  @spec reload(t, module) :: { :ok, t } | { :error, CoreKit.Structs.APIError.t }
  def reload(reloadable, repo)

  @doc """

  """
  @spec reload!(t, module) :: t
  def reload!(reloadable, repo)
end
defprotocol Manganese.EntityKit.Protocols.FilterableCollection do
  alias Manganese.EntityKit.Behaviors

  @doc """

  """
  @spec filter(t, (atom, Behaviors.Entity.id -> boolean)) :: t
  def filter(filterable, filter)
end
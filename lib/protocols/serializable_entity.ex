defprotocol Manganese.EntityKit.Protocols.SerializableEntity do
  @doc """

  """
  @spec serialize(t, atom) :: map
  def serialize(entity, entity_type)
end
defprotocol Manganese.EntityKit.Protocols.SerializableCollection do
  @doc """

  """
  @spec serialize(t, (term, atom -> map)) :: map
  def serialize(serializable, serializer)
end
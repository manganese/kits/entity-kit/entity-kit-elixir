defmodule Manganese.EntityKit.MixProject do
  use Mix.Project

  def project do
    [
      app: :manganese_entity_kit,
      version: "0.4.6",
      elixir: "~> 1.9",
      elixirc_paths: elixirc_paths(Mix.env),
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      package: package()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [ :logger ]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: [ "lib", "test/support" ]
  defp elixirc_paths(_),     do: [ "lib" ]

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # ExDoc
      { :ex_doc, "~> 0.19", only: :dev, runtime: false },

      # Ecto
      { :ecto, "~> 3.6" },

      # Manganese CoreKit
      { :manganese_core_kit, "~> 0.1" }
    ]
  end

  defp package do
    [
      name: "manganese_entity_kit",
      description: "Library for using ID-based entities and collections of entities",
      licenses: [ "Apache 2.0" ],
      links: %{
        "GitLab" => "https://gitlab.com/manganese/entity-kit/entity-kit-elixir"
      },
      maintainers: [
        "Samuel Goodell"
      ]
    ]
  end
end
